using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trampa : MonoBehaviour
{
    public Transform puntoA;
    public Transform puntoB;
    public float velocidad = 2.0f;
    public float tiempoDeEspera = 2.0f;

    private float distanciaAB;
    private bool enEspera = false;
    private float tiempoDeInicioEspera;

    void Start()
    {
        distanciaAB = Vector3.Distance(puntoA.position, puntoB.position);
    }

    void Update()
    {
        if (enEspera)
        {
            if (Time.time - tiempoDeInicioEspera >= tiempoDeEspera)
            {
                enEspera = false;
            }
            else
            {
                return;
            }
        }

        float distanciaRecorrida = Mathf.PingPong(Time.time * velocidad, distanciaAB);
        transform.position = Vector3.Lerp(puntoA.position, puntoB.position, distanciaRecorrida / distanciaAB);

        if (distanciaRecorrida >= distanciaAB)
        {
            enEspera = true;
            tiempoDeInicioEspera = Time.time;
        }
    }
}
