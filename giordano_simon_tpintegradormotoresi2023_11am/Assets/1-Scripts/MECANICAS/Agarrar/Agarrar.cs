using UnityEngine;


public class Agarrar : MonoBehaviour
{
    public GameObject AgarrarText;
    public GameObject ItemEnJugador;


    private void Start()
    {
        ItemEnJugador.SetActive(false);
        AgarrarText.SetActive(false);
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            AgarrarText.SetActive(true);

            if (Input.GetKey(KeyCode.E))
            {
                this.gameObject.SetActive(false);

                ItemEnJugador.SetActive(true);

                AgarrarText.SetActive(false);
            }
        }
    }
    void OnTriggerExit(Collider other)
    {
        AgarrarText.SetActive(false);
    }
}
