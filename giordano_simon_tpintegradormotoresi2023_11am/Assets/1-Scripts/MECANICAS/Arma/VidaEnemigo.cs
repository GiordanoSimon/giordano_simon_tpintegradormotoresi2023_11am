using UnityEngine;
using TMPro;

public class VidaEnemigo : MonoBehaviour
{
    public int maxHealth = 100;
    private int currentHealth;

    public static int killCounter = 0;
    public TextMeshProUGUI killCounterText;

    private void Start()
    {
        currentHealth = maxHealth;
    }

    public void TakeDamage(int damageAmount)
    {
        currentHealth -= damageAmount;

        if (currentHealth <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        killCounter++;
        UpdateKillCounterText();

        Destroy(gameObject);
    }

    private void UpdateKillCounterText()
    {
        if (killCounterText != null)
        {
            killCounterText.text = "Kills: " + killCounter.ToString();
        }
    }
}
