using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPausa : MonoBehaviour
{
    public GameObject menuDePausa;
    private bool menuOn;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!menuDePausa.activeSelf)
            {
                menuOn = !menuOn;

                if (menuOn)
                {
                    menuDePausa.SetActive(true);
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                    Time.timeScale = 0;
                }
                else
                {
                    MenuDeactivado();
                }
            }
        }
    }

    public void Continuar()
    {
        MenuDeactivado();
        menuOn = false;
    }

    public void VolverInicio()
    {
        SceneManager.LoadScene(0);
    }

    private void MenuDeactivado()
    {
        menuDePausa.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Time.timeScale = 1;
    }
}
