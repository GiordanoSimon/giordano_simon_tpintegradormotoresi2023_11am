using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PresurePlateClose: MonoBehaviour
{
    [SerializeField]
    GameObject door;

    bool isOpened = true;

    void OnTriggerEnter(Collider col)
    {
        if (isOpened == true)
        {
            isOpened = false;
            door.transform.position += new Vector3(0, -3, 0);
        }
    }
}
