using UnityEngine;

public class DebugMode : MonoBehaviour
{
    public Transform player;
    public Transform[] debugPoints;
    public KeyCode debugKey = KeyCode.X;

    private int currentDebugPointIndex = 0;

    private void Update()
    {
        if (Input.GetKeyDown(debugKey))
        {
            TeleportToDebugPoint();
        }
    }

    private void TeleportToDebugPoint()
    {
        if (debugPoints.Length == 0)
        {
            Debug.LogWarning("No hay puntos asignados.");
            return;
        }

        player.position = debugPoints[currentDebugPointIndex].position;
        currentDebugPointIndex = (currentDebugPointIndex + 1) % debugPoints.Length;
        Debug.Log("Player teletransportado a: " + debugPoints[currentDebugPointIndex].name);
    }
}
