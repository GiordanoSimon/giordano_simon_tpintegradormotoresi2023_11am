using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CambioEscena : MonoBehaviour
{
    public int NumeroEscena;

    public void cambiarEscena()
    {
        SceneManager.LoadScene(NumeroEscena);
    }

    public void Salir()
    {
        Application.Quit();
        Debug.Log("Juego Cerrado");
    }

}
