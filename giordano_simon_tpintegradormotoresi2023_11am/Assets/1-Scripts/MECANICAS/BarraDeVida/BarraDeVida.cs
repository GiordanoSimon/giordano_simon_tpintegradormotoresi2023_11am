using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraDeVida : MonoBehaviour
{
    public Image barraDeVida;
    public float VidaActual;
    public float VidaMaxima;

    void Start()
    {
        
        ActualizarBarraDeVida();
    }

    void Update()
    {
       
        VidaActual = Mathf.Clamp(VidaActual, 0, VidaMaxima);

        
        ActualizarBarraDeVida();
    }

    void ActualizarBarraDeVida()
    {
        
        VidaMaxima = Mathf.Max(0, VidaMaxima);
        VidaActual = Mathf.Clamp(VidaActual, 0, VidaMaxima);

        
        if (barraDeVida != null)
        {
            barraDeVida.fillAmount = VidaActual / VidaMaxima;
        }
    }
}
