using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{
    public Slider deslizadorVolumen;
    private AudioSource musicaFondo;

    private float volumenInicial;

    private void Start()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "Game")
        {
            AudioSource[] audioSources = scene.GetRootGameObjects()[0].GetComponentsInChildren<AudioSource>();

            foreach (AudioSource source in audioSources)
            {
                if (source.CompareTag("MusicaFondo"))
                {
                    musicaFondo = source;
                    break; 
                }
            }

            if (musicaFondo != null)
            {
                volumenInicial = PlayerPrefs.GetFloat("VolumenDeMusica", 0.5f);
                deslizadorVolumen.value = volumenInicial;

                EstablecerVolumen(volumenInicial);
                deslizadorVolumen.onValueChanged.AddListener(EstablecerVolumen);
            }
            else
            {
                Debug.LogWarning("No se encontr� el Audio.");
            }
        }
    }

    private void EstablecerVolumen(float volumen)
    {
        if (musicaFondo != null)
        {
            musicaFondo.volume = volumen;

            PlayerPrefs.SetFloat("VolumenDeMusica", volumen);
            PlayerPrefs.Save();
        }
        else
        {
            Debug.LogWarning("No se encontr� el Audio.");
        }
    }
}
