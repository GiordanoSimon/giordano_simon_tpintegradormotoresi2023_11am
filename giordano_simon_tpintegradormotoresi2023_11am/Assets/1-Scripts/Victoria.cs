using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Victoria : MonoBehaviour
{
    public GameObject textoVictoriaGO;
    private TextMeshPro textoVictoria;
    public AudioClip musicaVictoria;
    public AudioSource audioFondo;
    private AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        if (audioSource == null)
        {
            audioSource = gameObject.AddComponent<AudioSource>();
        }

        audioSource.clip = musicaVictoria;
        audioSource.loop = true;
        audioSource.playOnAwake = false;

        textoVictoria = textoVictoriaGO.GetComponent<TextMeshPro>();
        if (textoVictoria == null)
        {
            Debug.LogError("No se encontr�");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (audioFondo != null)
            {
                audioFondo.enabled = false;
            }

            textoVictoriaGO.SetActive(true);

            if (textoVictoria != null)
            {
                textoVictoria.text = "�Victoria!";
            }
            else
            {
                Debug.LogError("No se pudo");
            }

            audioSource.Play();
        }
    }
}
