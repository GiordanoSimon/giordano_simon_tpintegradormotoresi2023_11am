using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class ControlJugador1 : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;
    public int saludMaxima = 100; 

    public BarraDeVida barraDeVida; 

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        
        barraDeVida.VidaActual = barraDeVida.VidaMaxima;
    }

    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (saludMaxima <= 0)
        {
           
            ReiniciarEscena();
        }
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Trampa"))
        {
            
            ReducirSalud(25); 
        }
    }


    void ReducirSalud(int cantidad)
    {
        saludMaxima -= cantidad;

        
        barraDeVida.VidaActual = saludMaxima;

        
        Debug.Log("Salud reducida a " + saludMaxima);

        if (saludMaxima <= 0)
        {
            
            Debug.Log("�El jugador ha muerto!");
            ReiniciarEscena();
        }
    }

    void ReiniciarEscena()
    {
        
        int escenaActual = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(escenaActual);
    }
}
