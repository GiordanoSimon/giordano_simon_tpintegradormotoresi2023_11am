using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puerta : MonoBehaviour
{
    public Transform door;
    public Transform openTransform;
    public Transform closeTransform;
    public bool isUnlucked = true;
    void Start()
    {
        
        door.position = closeTransform.position;
    }

    
    void Update()
    {
       
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            
            door.position = openTransform.position;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            
            door.position = closeTransform.position;
        }
    }
}
