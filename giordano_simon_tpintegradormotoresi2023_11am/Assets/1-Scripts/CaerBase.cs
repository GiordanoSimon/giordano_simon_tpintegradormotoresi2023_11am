using UnityEngine;

public class CaerBase : MonoBehaviour
{
    public GameObject baseObj; 
    public float tiempoAntesDeCaer = 3.0f; 

    private bool cayendo = false;

    void OnTriggerEnter(Collider other)
    {
        if (cayendo)
            return;

        if (other.CompareTag("Base"))
        {
            cayendo = true;

            
            GetComponent<Rigidbody>().useGravity = false;
            GetComponent<Collider>().enabled = false;

          
            baseObj.GetComponent<Rigidbody>().useGravity = false;
            baseObj.GetComponent<Collider>().enabled = false;

            
            Invoke("CaerJugadorYBase", tiempoAntesDeCaer);
        }
    }

    void CaerJugadorYBase()
    {
        
        GetComponent<Rigidbody>().useGravity = true;
        GetComponent<Collider>().enabled = true;
        baseObj.GetComponent<Rigidbody>().useGravity = true;
        baseObj.GetComponent<Collider>().enabled = true;
    }
}
