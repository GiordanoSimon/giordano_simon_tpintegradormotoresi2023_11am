using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Salto : MonoBehaviour
{
    public float fuerzaSalto = 10f;
    public int maxDobleSalto = 1;

    private int dobleSaltosRealizados = 0;
    private bool enSuelo = false;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        dobleSaltosRealizados = 0;
    }

    void Update()
    {

        enSuelo = Physics.Raycast(transform.position, Vector3.down, 0.65f);


        if (enSuelo)
        {
            dobleSaltosRealizados = 0;
        }


        if (Input.GetButtonDown("Jump") && (enSuelo || dobleSaltosRealizados < maxDobleSalto))
        {
            Saltar();
        }
    }

    void Saltar()
    {
        rb.velocity = new Vector3(rb.velocity.x, 0f, rb.velocity.z);
        rb.AddForce(Vector3.up * fuerzaSalto, ForceMode.Impulse);

        if (!enSuelo)
        {
            dobleSaltosRealizados++;
        }
    }
}
